## 车牌识别工具

### 1.运行前置条件
> 安装 python、numpy、opencv-python、PIL


#### python安装方式自行百度

#### hyperlpr安装命令
> pip install hyperlpr

#### numpy安装命令：
>  pip install numpy

``` 注意：安装opencv-python是会附带安装numpy，所以可以只执行下面这条命令 ```

#### opencv-python安装命令：
>  pip install opencv-python


#### PIL安装命令：
> pip install Pillow

##### 电脑也需要安装openCV,安装方法：
> https://blog.csdn.net/weixin_40647819/article/details/79938325

##### 按照文章中配置好环境变量后，直接打开cmd，在CMD中输入一下命令：
``` C:\Users\Administrator>python
Python 3.7.3 (v3.7.3:ef4ec6ed12, Mar 25 2019, 22:22:05) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import cv2
```

`如果 没有报错，就说明配置成功`

##### 如果出现CV2找不到模块问题
``` ImportError: DLL load failed: 找不到指定的模块。 ```

##### 可使用docs文件夹中的depends22工具检测OpenCV的dll文件是否完整，不完整的可以在网上下载，然后放在C：/windows/system32文件夹下再试
``` 解决方案地址：https://blog.csdn.net/cskywit/article/details/81513066 ```40.

环境安装完成后，直接运行surface.py可看到初步运行效果

#####  测试图片都在res文件夹中


#### surface.py只是初步效果，最终效果可以看test.py